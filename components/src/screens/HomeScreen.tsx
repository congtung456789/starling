import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { User, vn, en } from '../data/Data';
import { useTheme, useLanguage } from '../../../App';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { UserType } from '../data/Data';
import { paramsRoot } from '../../StarlingApp';
import { Color } from '../../color';
const HomeScreen = () => {
  const navigation = useNavigation<NativeStackNavigationProp<paramsRoot>>();
  const isTheme = useTheme();
  const [allUser, setAllUser] = useState(User);
  const language = useLanguage();
  const changeLanguage = language ? en : vn;
  const toggleAddNewUser = (newUser: UserType) => {
    setAllUser([...allUser, newUser]);
  };
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.margin}>
          <Text
            style={[
              styles.titleYourContact,
              { color: isTheme ? Color.white : Color.black },
            ]}
          >
            {changeLanguage.titleYourContact}
          </Text>
        </View>
        <TouchableOpacity
          style={[styles.addNew, {}]}
          onPress={() =>
            navigation.navigate('AddNewContact', {
              toggleAddNewUser: toggleAddNewUser,
            })
          }
        >
          <View>
            <Ionicons
              name="add"
              size={35}
              color={isTheme ? Color.white : Color.howBlue}
            />
          </View>
          <View>
            <Text
              style={[
                styles.AddContact,
                {
                  color: isTheme ? Color.white : Color.howBlue,
                },
              ]}
            >
              {changeLanguage.txtAddNewContact}
            </Text>
          </View>
        </TouchableOpacity>
        {allUser.map((user) => {
          return (
            <View style={styles.user} key={user.id}>
              {user.uri !== '' ? (
                <Image
                  style={styles.avatar}
                  source={{
                    uri: user.uri,
                  }}
                />
              ) : (
                <View style={styles.avatar}>
                  <Icon name="user-alt" size={35} color="#F8F8FF" />
                </View>
              )}
              <View>
                <Text
                  style={[
                    styles.txtName,
                    { color: isTheme ? Color.white : Color.black },
                  ]}
                >
                  {user.name}
                </Text>
                <Text style={styles.txtEmail}>{user.email}</Text>
              </View>
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};
export default HomeScreen;
const styles = StyleSheet.create({
  AddContact: {
    fontSize: 20,
  },
  addNew: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 20,
    marginHorizontal: 10,
  },
  avatar: {
    alignItems: 'center',
    backgroundColor: Color.blueNhat,
    borderRadius: 50,
    height: 70,
    justifyContent: 'center',
    marginRight: 20,
    width: 70,
  },
  margin: {
    marginHorizontal: 25,
    marginVertical: 20,
  },
  titleYourContact: {
    fontSize: 25,
  },
  txtEmail: {
    color: Color.gray,
  },
  txtName: {
    fontSize: 20,
    marginBottom: 5,
  },
  user: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 20,
    marginHorizontal: 10,
  },
});
