import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Switch,
  Alert,
} from 'react-native';
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { vn, en } from '../data/Data';
import {
  useThemeUpdate,
  useTheme,
  useLanguage,
  useLanguageUpdate,
} from '../../../App';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import Modal from 'react-native-modal';
import { paramsRoot } from '../../StarlingApp';
import { Color } from '../../color';

function SettingsScreen() {
  const navigation = useNavigation<NativeStackNavigationProp<paramsRoot>>();
  const [isEnabled, setIsEnabled] = useState<boolean>(false);
  const language = useLanguage();
  const changeLanguage = language ? en : vn;
  const toggleSwitch = () => {
    setIsEnabled(!isEnabled);
  };
  const toggleTheme = useThemeUpdate();
  const toggleLanguage = useLanguageUpdate();
  const isTheme = useTheme();
  const ColorText = isTheme ? Color.white : Color.black;
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const createTwoButtonAlert = () =>
    Alert.alert(changeLanguage.txtSignOut, changeLanguage.titleSingOut, [
      {
        text: changeLanguage.cancel,
        onPress: () => {},
        style: 'cancel',
      },
      { text: changeLanguage.ok, onPress: () => navigation.navigate('SignIn') },
    ]);
  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <View style={styles.margin}>
            <Text
              style={[
                styles.titleSetting,
                {
                  color: ColorText,
                },
              ]}
            >
              {changeLanguage.titleSettings}
            </Text>
          </View>
          <View style={styles.merginVer}>
            <View style={styles.settings}>
              <View style={styles.flexDir}>
                <Icon name="cog" size={25} color={Color.howBlue} />
                <Text style={[styles.txtsetting, { color: ColorText }]}>
                  {changeLanguage.txtLanguage}
                </Text>
              </View>
              <TouchableOpacity onPress={toggleModal}>
                <Text style={[styles.txtsetting, { color: ColorText }]}>
                  {changeLanguage.languageChane}
                </Text>
                <Modal
                  isVisible={isModalVisible}
                  style={styles.viewModal}
                  onBackdropPress={toggleModal}
                >
                  <View style={styles.viewLanguage}>
                    <TouchableOpacity
                      style={styles.btnLanguage}
                      onPress={language === true ? toggleLanguage : toggleModal}
                    >
                      <Text style={styles.txtLang}>Tiếng Việt</Text>
                    </TouchableOpacity>
                    <View style={styles.bdwidth} />
                    <TouchableOpacity
                      style={styles.btnLanguage}
                      onPressIn={
                        language === false ? toggleLanguage : toggleModal
                      }
                    >
                      <Text style={styles.txtLang}>English</Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    style={styles.btnCancel}
                    onPress={toggleModal}
                  >
                    <Text style={[styles.txtLang, { color: Color.red }]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                </Modal>
              </TouchableOpacity>
            </View>
            <View style={styles.settings}>
              <View style={styles.flexDir}>
                <Icon name="cog" size={25} color="#356399" />
                <Text style={[styles.txtsetting, { color: ColorText }]}>
                  {changeLanguage.txtThemes}
                </Text>
              </View>
              <TouchableOpacity onPress={toggleTheme}>
                <Text style={[styles.txtsetting, { color: ColorText }]}>
                  {!isTheme ? changeLanguage.txtLight : changeLanguage.txtDark}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.settings}>
              <View style={styles.flexDir}>
                <Icon name="cog" size={25} color="#356399" />
                <Text style={[styles.txtsetting, { color: ColorText }]}>
                  {changeLanguage.txtNotifications}
                </Text>
              </View>
              <Switch
                trackColor={{ false: '#767577', true: '#356399' }}
                thumbColor={isEnabled ? '#356399' : '#f4f3f4'}
                ios_backgroundColor="red"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>
          <TouchableOpacity
            style={styles.settings}
            onPress={createTwoButtonAlert}
          >
            <Text style={[styles.txtsetting, { color: ColorText }]}>
              {changeLanguage.txtSignOut}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default SettingsScreen;

const styles = StyleSheet.create({
  bdwidth: {
    borderWidth: 0.2,
  },
  btnCancel: {
    alignItems: 'center',
    backgroundColor: Color.white,
    borderRadius: 10,
    height: 55,
    justifyContent: 'center',
    marginTop: 10,
  },
  btnLanguage: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  flexDir: {
    flexDirection: 'row',
  },
  margin: {
    marginHorizontal: 25,
    marginVertical: 20,
  },
  merginVer: {
    marginVertical: 60,
  },
  settings: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    marginHorizontal: 20,
  },
  titleSetting: {
    fontSize: 25,
  },
  txtLang: {
    color: Color.Xanh,
    fontSize: 18,
  },
  txtsetting: {
    fontSize: 20,
    marginBottom: 5,
    marginLeft: 20,
  },
  viewLanguage: {
    backgroundColor: Color.white,
    borderRadius: 10,
    height: 110,
  },
  viewModal: {
    justifyContent: 'flex-end',
    marginHorizontal: 20,
  },
});
