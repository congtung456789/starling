import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { vn, en } from '../data/Data';
import { useLanguage } from '../../../App';
import HomeScreen from './HomeScreen';
import SettingsScreen from './Settings';
import { Color } from '../../color';
function MyTabBar({ state, descriptors, navigation }: Props) {
  return (
    <View style={styles.flexDir}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={[
              styles.tabNavigate,
              {
                backgroundColor: isFocused ? Color.black : Color.white,
              },
            ]}
          >
            <Text
              style={[
                styles.label,
                { color: isFocused ? Color.white : Color.black },
              ]}
            >
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator<Props>();
type Props = {
  state: {};
  descriptors: {};
  navigation: {};
};
const Login: React.FC = () => {
  const language = useLanguage();
  const changeLanguage = language ? en : vn;
  return (
    <Tab.Navigator tabBar={(props) => <MyTabBar {...props} />}>
      <Tab.Screen
        name={changeLanguage.titleBottomContact}
        options={{ headerShown: false }}
        component={HomeScreen}
      />
      <Tab.Screen
        name={changeLanguage.titleBottomSettings}
        options={{ headerShown: false }}
        component={SettingsScreen}
      />
    </Tab.Navigator>
  );
};

export default Login;
const styles = StyleSheet.create({
  flexDir: { flexDirection: 'row' },
  label: {
    fontSize: 24,
  },
  tabNavigate: {
    alignItems: 'center',
    flex: 1,
    height: 60,
    justifyContent: 'center',
  },
});
