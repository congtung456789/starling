import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Pressable,
  Keyboard,
  SafeAreaView,
} from 'react-native';
import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useTheme, useLanguage } from '../../../App';
import { en, vn } from '../data/Data';
import { paramsRoot } from '../../StarlingApp';
import { Color } from '../../color';

const SignIn: React.FC = () => {
  const navigation = useNavigation<NativeStackNavigationProp<paramsRoot>>();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [ishide, setIshide] = useState<boolean>(true);
  const darkThem = useTheme();
  const language = useLanguage();
  const changeLanguage = language ? en : vn;
  const themStyles = {
    backgroundColor: darkThem ? Color.white : Color.howBlue,
    color: darkThem ? Color.white : Color.black,
  };
  return (
    <SafeAreaView>
      <Pressable onPress={Keyboard.dismiss}>
        <View style={styles.marginContainer}>
          <Text
            style={[
              styles.titleSingin,
              { color: !darkThem ? Color.black : Color.white },
            ]}
          >
            {changeLanguage.titleSingIN}
          </Text>
          <View style={styles.inputTxt}>
            <Text style={{ color: themStyles.color }}>
              {changeLanguage.txtEmail}
            </Text>

            <TextInput
              style={{ color: themStyles.color }}
              value={email}
              onChangeText={setEmail}
            />
          </View>
          <View style={styles.inputPassword}>
            <View>
              <Text style={{ color: themStyles.color }}>
                {changeLanguage.txtPassword}
              </Text>
              <TextInput
                style={{ color: themStyles.color }}
                value={password}
                onChangeText={setPassword}
                secureTextEntry={ishide}
              />
            </View>
            <TouchableOpacity onPress={() => setIshide(!ishide)}>
              <Icon
                name={ishide ? 'eye' : 'eye-slash'}
                size={20}
                color={themStyles.color}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.fogotpass}>
            <Text style={{ color: themStyles.color }}>
              {changeLanguage.txtForGotPassword}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.btnSingIn,
              { backgroundColor: themStyles.backgroundColor },
            ]}
            onPress={() => navigation.navigate('Login')}
          >
            <Text style={styles.txtSingIn}>{changeLanguage.txtSignIN}</Text>
          </TouchableOpacity>
        </View>
      </Pressable>
    </SafeAreaView>
  );
};
export default SignIn;
const styles = StyleSheet.create({
  btnSingIn: {
    alignItems: 'center',
    borderRadius: 10,
    height: 70,
    justifyContent: 'center',
    marginTop: 20,
    width: '100%',
  },
  fogotpass: {
    alignItems: 'flex-end',
    width: '100%',
  },
  inputPassword: {
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    height: 70,
    justifyContent: 'space-between',
    marginVertical: 10,
    paddingHorizontal: 15,
    width: '100%',
  },
  inputTxt: {
    borderRadius: 10,
    borderWidth: 1,
    height: 70,
    justifyContent: 'center',
    marginVertical: 10,
    paddingHorizontal: 15,
    width: '100%',
  },
  marginContainer: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  titleSingin: {
    fontSize: 26,
    marginVertical: 80,
  },
  txtSingIn: {},
});
