import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome5';
import * as ImagePicker from 'expo-image-picker';
import * as Location from 'expo-location';
import { User, vn, en } from '../data/Data';
import { useTheme, useLanguage } from '../../../App';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { paramsRoot } from '../../StarlingApp';
import { Color } from '../../color';
type Props = NativeStackScreenProps<paramsRoot, 'AddNewContact'>;
const AddNewContact = ({ route }: Props) => {
  console.log('route', route);
  const [fullname, setFullName] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [nickname, setNickname] = useState<string>('');
  const [image, setImage] = useState<string>();
  const [locationNum, setLocationNum] = useState<string>();
  const isTheme = useTheme();
  const language = useLanguage();
  const changeLanguage = language ? en : vn;
  useEffect(() => {
    (async () => {
      let locations = await Location.getCurrentPositionAsync({});
      setLocationNum(locations);
      console.log(locations);
    })();
  }, []);
  const openImagePickerAsync = async () => {
    const pickerResult = await ImagePicker.launchImageLibraryAsync();
    setImage(pickerResult.uri);
  };
  const addNew = () => {
    const newUser = User.push({
      id: Math.floor(Math.random() * 10000),
      name: fullname,
      email: nickname,
      uri: image,
    });
    return route.params.toggleAddNewUser(newUser);
  };
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.marginContainer}>
          <Text
            style={[
              styles.title,
              { color: isTheme ? Color.white : Color.black },
            ]}
          >
            {changeLanguage.titleAddEditContact}
          </Text>
          <TouchableOpacity onPress={openImagePickerAsync}>
            {image ? (
              <Image
                style={styles.avatar}
                source={{
                  uri: image,
                }}
              />
            ) : (
              <View>
                <View
                  style={[
                    styles.editAvatar,
                    { backgroundColor: isTheme ? Color.white : Color.howBlue },
                  ]}
                >
                  <Icon
                    name="pen"
                    size={15}
                    color={isTheme ? Color.black : Color.white}
                  />
                </View>
                <View style={styles.avatar}>
                  <Icon name="user-alt" size={35} color="#F8F8FF" />
                </View>
              </View>
            )}
          </TouchableOpacity>
          <Text style={{ color: isTheme ? Color.white : Color.black }}>
            {changeLanguage.txtFullname}
          </Text>
          <View style={styles.inputtxt}>
            <TextInput
              style={{ color: isTheme ? Color.white : Color.black }}
              value={fullname}
              placeholder="Full name"
              onChangeText={(val) => setFullName(val)}
            />
          </View>
          <Text style={{ color: isTheme ? Color.white : Color.black }}>
            {changeLanguage.txtPhone}
          </Text>
          <View style={styles.inputtxt}>
            <TextInput
              style={{ color: isTheme ? Color.white : Color.black }}
              value={phone}
              placeholder="Phone number"
              onChangeText={(val) => setPhone(val)}
              keyboardType="numeric"
            />
          </View>
          <Text style={{ color: isTheme ? Color.white : Color.black }}>
            {changeLanguage.txtNickName}
          </Text>
          <View style={styles.inputtxt}>
            <TextInput
              style={{ color: isTheme ? Color.white : Color.black }}
              value={nickname}
              placeholder="Nickname"
              onChangeText={(val) => setNickname(val)}
            />
          </View>
          <Text style={{ color: isTheme ? Color.white : Color.black }}>
            {changeLanguage.txtLocation}
          </Text>
          <View style={styles.inputtxt}>
            <Text>
              {locationNum ? (
                <>
                  <Text
                    style={{ color: isTheme ? Color.white : Color.black }}
                  >{`${locationNum.coords.latitude}, ${locationNum.coords.longitude}`}</Text>
                </>
              ) : null}
            </Text>
          </View>
          <View style={styles.styleBtn}>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: isTheme ? Color.white : Color.howBlue },
              ]}
              onPress={addNew}
            >
              <Text
                style={[
                  styles.singin,
                  { color: isTheme ? Color.black : Color.white },
                ]}
              >
                {changeLanguage.btnAdd}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AddNewContact;

const styles = StyleSheet.create({
  avatar: {
    alignItems: 'center',
    backgroundColor: Color.blueNhat,
    borderRadius: 50,
    height: 100,
    justifyContent: 'center',
    marginRight: 20,
    width: 100,
  },
  button: {
    alignItems: 'center',
    backgroundColor: Color.howBlue,
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    width: '40%',
  },
  editAvatar: {
    alignItems: 'center',
    backgroundColor: Color.howBlue,
    borderRadius: 50,
    height: 40,
    justifyContent: 'center',
    left: 65,
    marginRight: 20,
    position: 'absolute',
    top: 2,
    width: 40,
    zIndex: 100,
  },
  inputtxt: {
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    justifyContent: 'center',
    marginVertical: 10,
    paddingHorizontal: 15,
    width: '100%',
  },
  marginContainer: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  singin: {
    color: Color.white,
    fontSize: 16,
  },

  styleBtn: {
    alignItems: 'flex-end',
    width: '100%',
  },
  title: {
    fontSize: 26,
    marginVertical: 10,
  },
});
