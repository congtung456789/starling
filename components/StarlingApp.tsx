import React from 'react';
import {
  NavigationContainer,
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme,
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  Provider as PaperProvider,
  MD2DarkTheme as PaperDarkTheme,
  DefaultTheme as PaperDefaultTheme,
} from 'react-native-paper';
import Login from './src/screens/LogIn';
import AddNewContact from './src/screens/AddNewContact';
import { useTheme } from '../App';
import SignIn from './src/screens/SingIn';
export type paramsRoot = {
  SignIn: undefined;
  AddNewContact: { toggleAddNewUser: () => void };
  Login: undefined;
};
const Stack = createNativeStackNavigator<paramsRoot>();

const StarlingApp: React.FC = () => {
  const darkTheme = useTheme();
  const CustomDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
    },
  };
  const CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
      ...NavigationDarkTheme.colors,
      ...PaperDarkTheme.colors,
    },
  };
  const theme = darkTheme ? CustomDarkTheme : CustomDefaultTheme;
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer theme={theme}>
        <Stack.Navigator>
          <Stack.Screen
            name="SignIn"
            options={{ headerShown: false }}
            component={SignIn}
          />
          <Stack.Screen
            name="Login"
            options={{ title: '', headerShown: false }}
            component={Login}
          />
          <Stack.Screen
            name="AddNewContact"
            options={{ title: '' }}
            component={AddNewContact}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

export default StarlingApp;
