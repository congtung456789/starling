import React, { useContext, useState } from 'react';
import StarlingApp from './components/StarlingApp';
const ThemeContenxt = React.createContext();
const ThemeUpdateContext = React.createContext();
const Language = React.createContext();
const LanguageUpdate = React.createContext();
export function useTheme() {
  return useContext(ThemeContenxt);
}
export function useThemeUpdate() {
  return useContext(ThemeUpdateContext);
}
export function useLanguage() {
  return useContext(Language);
}
export function useLanguageUpdate() {
  return useContext(LanguageUpdate);
}
const App = () => {
  const [theme, setTheme] = useState<boolean>(false);
  const [language, setLanguage] = useState<boolean>(false);
  const toggleTheme = () => {
    setTheme(!theme);
  };
  const toggleLanguage = () => {
    setLanguage(!language);
  };
  return (
    <ThemeContenxt.Provider value={theme}>
      <ThemeUpdateContext.Provider value={toggleTheme}>
        <Language.Provider value={language}>
          <LanguageUpdate.Provider value={toggleLanguage}>
            <StarlingApp />
          </LanguageUpdate.Provider>
        </Language.Provider>
      </ThemeUpdateContext.Provider>
    </ThemeContenxt.Provider>
  );
};
export default App;
